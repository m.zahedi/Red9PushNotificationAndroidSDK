package com.rasha.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import org.eclipse.paho.android.service.MqttService;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * Created by milad zahedi on 10/2/17.
 *
 */

public abstract class Red9PushService extends MqttService implements MqttCallback ,PushReceived {
    public final static String TAG = "Red9Push";

    @Override
    public void onCreate() {

        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "initialising onStartCommand");

        try {
            String token = getAndSaveToken(intent);
            String clientId = createVerifiedClientId();
            Log.d(TAG, "token " + token + " client id " + clientId );
            MqttClient client = new MqttClient("tcp://192.168.1.168:1883", clientId, new MemoryPersistence());
            client.setCallback(this);
            client.connect();

            client.unsubscribe(token);
            client.subscribe(token);

        } catch (MqttException e) {
            e.printStackTrace();
        }
        //return super.onStartCommand(intent, flags, startId);
        //return START_REDELIVER_INTENT;
        return START_STICKY_COMPATIBILITY;

    }
    private String getAndSaveToken(Intent intent) {
        SharedPreferences prefs = this.getSharedPreferences("InstallationToken", 0);
        if(intent != null && intent.hasExtra("token")){
            String token = intent.getStringExtra("token");

            prefs.edit().putString("token", token).apply();
            return token;
        } else {
            return prefs.getString("token", "token_not_set");

        }
    }

    private String createVerifiedClientId() {
        SharedPreferences prefs = this.getSharedPreferences("InstallationToken", 0);
        String not_set = "NOT_SET";
        String clientId;
        clientId = prefs.getString("id", not_set);

        if (clientId.equals(not_set)) {
            clientId = MqttClient.generateClientId();
            Log.d(TAG, "Creating keys for 1st time: "+ clientId);
            prefs.edit().putString("id", clientId).apply();
        }
        return clientId;
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");

        return super.onBind(intent);
    }

    @Override
    public void connectionLost(Throwable cause) {
        Log.d(TAG, "connectionLost");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        pushReceived(message);
    }





    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        Log.d(TAG, "deliveryComplete token => " + token);
    }


    public static void init(Context context, Class<? extends Red9PushService> className, String token) {
        Intent intent = new Intent(context, className);
        intent.putExtra("token",token);
        context.startService(intent);
    }
}

interface PushReceived {
    void pushReceived(MqttMessage message);
}