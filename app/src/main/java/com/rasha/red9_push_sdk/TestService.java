package com.rasha.red9_push_sdk;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.rasha.sdk.Red9PushService;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by milad zahedi on 10/2/17.
 *
 */

public class TestService extends Red9PushService {

    @Override
    public void pushReceived(MqttMessage message) {
        try {
            pushNotification(this,message);
        } catch (Exception e) {
            Log.d(TAG,e.toString());
        }

    }

    private void showToast(MqttMessage message){
        Handler handler = new Handler(Looper.getMainLooper());
        final String str = message.toString();
        handler.post(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),
                        str,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void pushNotification(Context context,MqttMessage message)
    {
        NotificationManager nm = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,0,notificationIntent,0);

        //set
        builder.setContentIntent(contentIntent);
        builder.setContentText(message.toString());
        builder.setContentTitle("title");
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        nm.notify((int)System.currentTimeMillis(),notification);
    }


}
