package com.rasha.red9_push_sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.rasha.sdk.Red9PushService;


public class MainActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Red9PushService.init(this, TestService.class,"token");

    }

}
